let Consultant = require("../models").Consultant;
let moment = require("moment");

module.exports = class ConsultantsRepository {

    constructor(ds){
        this._ds = ds;
    }

    list(){
        return new Promise((resolve, reject) => {
            let results = [];
            this._ds.getConnection((err1, connection) => {
                if (err1){
                    reject(new Error(err1.message));
                }
                connection.query("select * from consultants", (err2, rows, fields) => {
                    if (err2){
                        reject(new Error(err2.message));
                    }
                    connection.release();

                    if (rows.length > 0){
                        for (let index = 0; index < rows.length; index++){
                            let consultant = new Consultant();
                            for(let index2 = 0; index2 < fields.length; index2++){
                                if (fields[index2].name === "createdDate" || fields[index2].name === "modifiedDate"){
                                    consultant[fields[index2].name] = (rows[index][fields[index2].name]) ? 
                                        moment(rows[index][fields[index2].name]).format("DD-MM-YYYY HH:mm:ss"): null;
                                } 
                                else {
                                    consultant[fields[index2].name] = rows[index][fields[index2].name];
                                }
                            }
                            results.push(consultant);
                        }
                    }
                    resolve(results);
                });
            });
        });
    }

    add(consultant){
        return new Promise((resolve, reject) => {
            let createdDate = new Date();
            let data = [
                consultant.firstName, 
                consultant.lastName, 
                consultant.email, 
                consultant.address, 
                createdDate
            ];
            let sql = "insert into consultants (firstName, lastName, email, address, createdDate) values (?, ?, ?, ?, ?)";
            this._ds.getConnection((err1, connection) => {
                if (err1){
                    reject(new Error(err1.message));
                }
                connection.query(sql, data, (err2, rows) => {
                    if (err2){
                        reject(new Error(err2.message));
                    }
                    connection.release();
                    resolve({
                        id: rows.insertId,
                        createdDate: moment(createdDate).format("DD-MM-YYYY HH:mm:ss"),
                        modifiedDate: null
                    });
                });
            });
        });
    }

    set(consultant){
        return new Promise((resolve, reject) => {
            let modifiedDate = new Date();
            let data = [
                consultant.firstName, 
                consultant.lastName, 
                consultant.email, 
                consultant.address, 
                modifiedDate,
                consultant.id
            ];
            let sql = "update consultants set firstName = ?, lastName = ?, email = ?, address = ?, modifiedDate = ? where id = ?";
            this._ds.getConnection((err1, connection) => {
                if (err1){
                    reject(new Error(err1.message));
                }
                connection.query(sql, data, (err2, rows) => {
                    if (err2){
                        reject(new Error(err2.message));
                    }
                    connection.release();
                    resolve({
                        modifiedDate: moment(modifiedDate).format("DD-MM-YYYY HH:mm:ss")
                    });
                });
            });                     
        });
    }

    get(id){
        return new Promise((resolve, reject) => {
            let data = [id];
            this._ds.getConnection((err1, connection) => {
                if (err1){
                    reject(new Error(err1.message));
                }
                connection.query("select * from consultants where id = ?", data, (err2, rows, fields) => {
                    if (err2){
                        reject(new Error(err2.message));
                    }
                    connection.release();
                    let consultant = new Consultant();
                    if (rows.length > 0){
                        for (let index = 0; index < rows.length; index++){
                            for(let index2 = 0; index2 < fields.length; index2++){
                                if (fields[index2].name === "createdDate" || fields[index2].name === "modifiedDate"){
                                    consultant[fields[index2].name] = (rows[index][fields[index2].name]) ? 
                                        moment(rows[index][fields[index2].name]).format("DD-MM-YYYY HH:mm:ss"): null;
                                } 
                                else {
                                    consultant[fields[index2].name] = rows[index][fields[index2].name];
                                }
                            }
                        }
                    }
                    resolve(consultant);
                });
            });
        });
    }

    remove(id){
        return new Promise((resolve, reject) => {
            let sql = "delete from consultants where id = ?";
            let data = [id];
            this._ds.getConnection((err1, connection) => {
                if (err1){
                    reject(new Error(err1.message));
                }
                connection.query(sql, data, (err2) => {
                    if (err2){
                        reject(new Error(err2.message));
                    }
                    connection.release();
                    resolve();
                });
            });                     
        });
    }
}