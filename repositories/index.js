let mysql = require("mysql");
let ConsultantRepository = require("./consultants");

let ds;

if (!ds){
    ds = mysql.createPool({
        connectionLimit : process.env.LIMIT || 10,
        host            : process.env.DB_HOST ||'192.168.99.100',
        user            : process.env.DB_USER || 'node',
        password        : process.env.DB_PASS || '1234',
        database        : process.env.DB || 'consultants'
    });
}

module.exports.consultantRepository = new ConsultantRepository(ds);