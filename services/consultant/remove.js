let repository = require("../../repositories").consultantRepository;

module.exports = class RemoveById{

    constructor(ee){
        this._ee = ee;
        this._ee.on("consultant:remove", (entity) => {
            repository.remove(entity.id)
                .then(() => {
                    this._ee.emit("consultant:remove:success");
                }, (err)  => {
                    this._ee.emit("consultant:remove:unsuccess", err);
                });        
        });
    }
    
}